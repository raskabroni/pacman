//
//  Collision.cpp
//  pacman_xcode
//
//  Created by Dmitry Basavin on 23.05.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "Collision.h"

int CheckCollision(HitBox first, HitBox second){
  if (first.x < second.x + second.w && first.x + first.w > second.x &&
      first.y < second.y + second.h && first.y + first.h > second.y) {
    return 1;
  }
  
  return 0;
}