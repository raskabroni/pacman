//
//  LTexture.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__LTexture__
#define __pacman__LTexture__

#include <stdio.h>
#include <iostream>
#include <SDL2/SDL.h>

//Texture wrapper class
class LTexture {
  
public:
  //Initializes variables
  LTexture();
  
  //Deallocates memory
  ~LTexture();
  
  // Set internal renderer
  void setRenderer( SDL_Renderer* render);
  
  //Loads image at specified path
  bool loadFromFile( std::string path );
  
  //Deallocates texture
  void free();
  
  //Set color modulation
  void setColor( Uint8 red, Uint8 green, Uint8 blue );
  
  //Set blending
  void setBlendMode( SDL_BlendMode blending );
  
  //Set alpha modulation
  void setAlpha( Uint8 alpha );
  
  //Renders texture at given point
  void render( int x, int y, SDL_Rect* clip = NULL );
		
  void render( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip );
  
  //Gets image dimensions
  int getWidth();
  int getHeight();
  
private:
  
  //The actual hardware texture
  SDL_Texture* mTexture;
  
  //Hardware renderer
  SDL_Renderer* renderer = NULL;
  
  //Image dimensions
  int mWidth;
  int mHeight;
};

#endif /* defined(__pacman__LTexture__) */
