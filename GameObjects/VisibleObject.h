//
//  VisibleObject.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__VisibleObject__
#define __pacman__VisibleObject__

#include <iostream>
#include <SDL2/SDL.h>
#include <string.h>
#include "GameObject.h"
#include "Interfaces/IRenderable.h"
#include "../LTexture.h"
#include "../dataTypes.h"

// Классы объектов
// Класс для видимых объектов
class VisibleObject: public GameObject, public IRenderable {
public:
  
  std::string clName;
  Coords position;
  Dimensions dimensions;
  
  LTexture sprite;
  SDL_Rect* spriteClips;
  int framesCount;
  
  HitBox GetHitBox();
  
  int render(int frame);
  int render(int frame, double angle, SDL_RendererFlip flipType);
  
  VisibleObject(SDL_Renderer* renderer, Coords pos, std::string image_path);
  ~VisibleObject();
};

#endif /* defined(__pacman__VisibleObject__) */
