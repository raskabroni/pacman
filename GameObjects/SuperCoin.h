//
//  SuperCoin.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__SuperCoin__
#define __pacman__SuperCoin__

#include <SDL2/SDL.h>
#include "VisibleObject.h"
#include "../dataTypes.h"

class SuperCoin: public VisibleObject {
public:
  SuperCoin(SDL_Renderer* renderer, Coords pos);
  ~SuperCoin();
};

#endif /* defined(__pacman__SuperCoin__) */
