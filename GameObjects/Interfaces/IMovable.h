//
//  IMovable.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__IMovable__
#define __pacman__IMovable__

enum MOVE_DIRECTIONS {
  MOVE_UP,
  MOVE_RIGHT,
  MOVE_DOWN,
  MOVE_LEFT
};

// Интерфейс для метода перемещения объектов (move)
class IMovable {
public:
  int virtual move(MOVE_DIRECTIONS direction) = 0;
};

#endif /* defined(__pacman__IMovable__) */
