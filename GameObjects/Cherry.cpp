//
//  Cherry.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "Cherry.h"

Cherry::Cherry(SDL_Renderer* renderer, Coords pos):VisibleObject (renderer, pos, "sprites/cherry.png"){
  clName = "Cherry";
  
  dimensions.width = 35;
  dimensions.height = 35;
  
  spriteClips = new SDL_Rect[1];
  
  framesCount = 1;
  
  spriteClips[ 0 ].x = 0;
  spriteClips[ 0 ].y = 0;
  spriteClips[ 0 ].w = 35;
  spriteClips[ 0 ].h = 35;
}

Cherry::~Cherry(){
  delete[] spriteClips;//free(spriteClips);
}
