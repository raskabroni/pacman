//
//  Ghost.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__Ghost__
#define __pacman__Ghost__

#include "Character.h"

class Ghost: public Character {
public:
  MOVE_DIRECTIONS moveDirection;
  
  Ghost(SDL_Renderer* renderer, Coords pos, std::string image_path);
  int move();
  ~Ghost();
};

#endif /* defined(__pacman__Ghost__) */
